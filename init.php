<?php

use Hyphenation\src\AdditionalFunctions\TimeCounter;

use Hyphenation\src\MainFunctions\HyphenatedWord;

use Hyphenation\src\AdditionalFunctions\Logger;

use Hyphenation\src\AdditionalFunctions\Cache;



use Hyphenation\src\AdditionalFunctions\Printing;

use Hyphenation\src\API\Api;

use Hyphenation\src\MainFunctions\HyphenatedWordProxy;

use Hyphenation\src\Databases\SqlQueryBuilderInterface;



require_once __DIR__ . '/vendor/autoload.php';

$print = new Printing();

$timeFromFile = new TimeCounter();
$timeFromFile->startTime();

//$cache= new Cache();


//$cache->useDatabaseAsCache();

$proxy = new HyphenatedWord();

$proxy->inputWordsAndFragments("f","f");

$proxy->setHyphenatedWords();

//$database->uploadHyphenatedWords($wordFromFile->getHyphenatedWords());

$print->printListOfHyphenatedWords($proxy->getHyphenatedWords());

$print->printIterationNumber();
$print->printDuration($timeFromFile->getDuration());

$logFile = new Logger();
$logFile->logListOfHyphenatedWords($proxy->getHyphenatedWords());
$logFile->logIterations();
$logFile->logTime($timeFromFile->getDuration());


$api = new Api();
$api->useApi();

