FROM ubuntu:latest

ENV TZ=Europe/Vilnius
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update 
RUN apt-get install -y apache2 
RUN apt-get clean
RUN apt-get install -y git 
RUN apt-get install -y php
RUN apt-get install -y composer 
RUN apt-get install -y php7.2-xml  
RUN apt-get install -y php7.2-mysql 

RUN git clone https://github.com/vgumonis/datamanipulation.git temp
RUN mv -v /temp/* /var/www/html
RUN rm -rf temp

RUN cd /var/www/html && composer install --no-dev
RUN rm /var/www/html/index.html

ENTRYPOINT service apache2 start && /bin/bash

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2

EXPOSE 80
CMD apachectl -D FOREGROUND
