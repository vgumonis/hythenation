<?php

use PHPUnit\Framework\TestCase;

use Hyphenation\src\Algorithm\SyllabificatedWord;
use Hyphenation\Resources\Resources;


class HyphenationTest extends TestCase
{

    public $testWord;
    public $wordFragments;

    public function setUp()
    {
        $this->testWord = new SyllabificatedWord();
        $this->wordFragments = explode("\n", file_get_contents(Resources::getFragmentFile()));
    }

    public function testMistranslateWordHyphenation()
    {
        $mistranslate = $this->testWord->hyphenate("mistranslate", $this->wordFragments);
        $this->assertEquals("mis-trans-late", $mistranslate, "word hyphenated wrong");
    }


    public function testAlphabeticalWordHyphenation()
    {
        $alphabetical = $this->testWord->hyphenate("alphabetical", $this->wordFragments);
        $this->assertEquals("al-pha-bet-i-cal", $alphabetical, "word hyphenated wrong");
    }



    public function testButtonsWordHyphenation()
    {
        $buttons = $this->testWord->hyphenate("buttons", $this->wordFragments);
        $this->assertEquals("but-ton-s", $buttons, "word hyphenated wrong");

    }

    public function testHovercraftWordHyphenation()
    {
        $hovercraft = $this->testWord->hyphenate("hovercraft", $this->wordFragments);
        $this->assertEquals("hov-er-craft", $hovercraft, "word hyphenated wrong");

    }

}



