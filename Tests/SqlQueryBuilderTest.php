<?php
/**
 * Created by PhpStorm.
 * User: vilius
 * Date: 18.10.23
 * Time: 10.44
 */


use PHPUnit\Framework\TestCase;
use Hyphenation\src\Databases\SqlQueryBuilder;


class SqlQueryBuilderTest extends TestCase
{
    public $query;

    function setUp()
    {
        $this->query = new SqlQueryBuilder();
    }

    public function testSqlSelectAll()
    {
        $this->query->select();
        $this->assertEquals("SELECT * ", $this->query->query, "select all is wrong");
    }

    public function testSqlSelectMany()
    {
        $this->query->select(["id", "word", "hyphenated_word"]);
        $this->assertEquals("SELECT id , word , hyphenated_word  ", $this->query->query, "select many is wrong");
    }


    public function testSqlFrom()
    {
        $this->query->from("hyphenation");
        $this->assertEquals("FROM hyphenation", $this->query->query, "from is wrong");
    }

    public function testSqlWhere()
    {
        $this->query->where(["word" => "mistranslate", "hyphenated_word" => "mis-trans-late"]);
        $this->assertEquals(" WHERE (word = \"mistranslate\" AND  hyphenated_word = \"mis-trans-late\")",
            $this->query->query, "where is wrong");
    }

    public function testSqlInsertInto()
    {
        $this->query->insertInto("hyphenation", ["word", "hyphenated_word"]);
        $this->assertEquals("INSERT INTO hyphenation (word , hyphenated_word)", $this->query->query,
            "insertInto is wrong");
    }

    public function testSqlValue()
    {
        $this->query->value(["mistranslate", "mis-trans-late"]);
        $this->assertEquals(" VALUES ('mistranslate' , 'mis-trans-late')", $this->query->query, "value is wrong");
    }

    public function testSqlDeleteFrom()
    {
        $this->query->deleteFrom("hyphenation");
        $this->assertEquals("DELETE FROM hyphenation", $this->query->query, "delete is wrong");
    }


    public function testSqlUpdate()
    {
        $this->query->update("hyphenation");
        $this->assertEquals(" UPDATE hyphenation", $this->query->query, "update is wrong");
    }

    public function testSqlSet()
    {
        $this->query->set(["word" => "mistranslate", "hyphenated_word" => "mis-trans-late"]);
        $this->assertEquals(" SET word = \"mistranslate\" , hyphenated_word = \"mis-trans-late\"", $this->query->query,
            "set is wrong");
    }

    public function testSqlQuery1()
    {
        $this->query->select()->from("hyphenation")->where([
            "word" => "mistranslate",
            "hyphenated_word" => "mis-trans-late"
        ]);
        $this->assertEquals("SELECT * FROM hyphenation WHERE (word = \"mistranslate\" AND  hyphenated_word = \"mis-trans-late\")",
            $this->query->query, "query is wrong");
    }

    public function testSqlQuery2()
    {
        $this->query->insertInto("hyphenation", ["word", "hyphenated_word"])->value(["mistranslate", "mis-trans-late"]);
        $this->assertEquals("INSERT INTO hyphenation (word , hyphenated_word) VALUES ('mistranslate' , 'mis-trans-late')",
            $this->query->query, "query is wrong");
    }
}
