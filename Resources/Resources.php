<?php
/**
 * Created by PhpStorm.
 * User: vilius
 * Date: 18.10.17
 * Time: 14.05
 */

namespace Hyphenation\Resources;

class Resources
{

    private static $wordFile = __DIR__. "/words.txt";
    private static $fragmentsFile = __DIR__. "/tex-hyphenation-patterns.txt";
    private static $cacheFile = __DIR__. "/cache.txt";
    private static $databaseConfig = __DIR__. "/databaseConfig.xml";


    public static function getWordFile()
    {
        return self::$wordFile;
    }


    public static function getFragmentFile()
    {
        return self::$fragmentsFile;
    }


    public static function getCacheFile()
    {
        return self::$cacheFile;
    }

    public static function getDatabaseConfig()
    {
        return self::$databaseConfig;
    }
}
