<?php



use Hyphenation\src\ConsoleAppController\ConsoleAppController;

require_once __DIR__ . '/vendor/autoload.php';

$app = new ConsoleAppController();

//$app->resetWordFragments();
//$app->resetWordsToTest();

$app->inputSource();
$app->fragmentSource();
$app->cacheSource();
$app->runAlgorithm();

//$app->resetHyphenatedWords();

