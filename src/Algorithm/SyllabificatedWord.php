<?php

namespace Hyphenation\src\Algorithm;

class SyllabificatedWord extends FoundPatterns
{


    /**
     *
     * @param string $fragment
     * @return array Of number and it's position in specific fragment
     */
    private function getFragmentNumberAndIntex($fragment)
    {
        $arrayOfFragmentElements = str_split($fragment);
        $indexOfElement = 0;
        $arrayOfFrangmentsWithIndex = [];

        foreach ($arrayOfFragmentElements as $element) {
            if (is_numeric($element)) {
                $arrayOfFrangmentsWithIndex[$indexOfElement] = $element;
                continue;
            }
            $indexOfElement++;
        }
        return $arrayOfFrangmentsWithIndex;
    }


    /**
     * @param string $enteredWord
     * @param array $wordFragments
     * @return array Of numbers to insert.
     */
    private function getValuesToInsert($enteredWord, $wordFragments)
    {
        $patternMatches = $this->getPatternMatches($enteredWord, $wordFragments);
        $valuesToInsert = array_fill(0, strlen($enteredWord) + 1, null);

        foreach ($patternMatches as $position => $fragment) {
            if ($fragment !== null) {
                foreach ($this->getFragmentNumberAndIntex($fragment) as $key => $value) {
                    if ($valuesToInsert [$key + $position - 1] < $value || $valuesToInsert [$key + $position - 1] === null) {
                        $valuesToInsert [$key + $position - 1] = $value;

                    }
                }
            }
        }
        return $valuesToInsert;
    }


    /**
     * @param string $enteredWord
     * @param array $wordFragments
     * @return array  Word with inserted number
     */
    private function getWordJugle($enteredWord, $wordFragments)
    {
        $valuesToInsert = $this->getValuesToInsert($enteredWord, $wordFragments);
        $wordJugle = [];
        $valueIndex = 0;
        $splitEnteredWord = str_split($enteredWord);

        foreach ($splitEnteredWord as $letter) {

            if ($valuesToInsert[$valueIndex] !== null) {
                $wordJugle[] = $valuesToInsert[$valueIndex];
            }
            $valueIndex++;
            $wordJugle[] = $letter;
        }
        return $wordJugle;
    }


    /**
     * @param string $enteredWord
     * @param array $wordFragments
     * @return string Returns syllabificated word with inserted hyphens
     */
    public function hyphenate($enteredWord, $wordFragments): string
    {
        $wordJugle = $this->getWordJugle($enteredWord, $wordFragments);
        $cleanJugle = [];
        foreach ($wordJugle as $value) {

            if (is_numeric($value)) {
                if ($value % 2 !== 0) {
                    $cleanJugle[] = "-";
                }
            } else {
                $cleanJugle[] = $value;
            }
        }
        $answer = implode("", $cleanJugle);
        return $answer;
    }

}

