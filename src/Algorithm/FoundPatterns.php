<?php

namespace Hyphenation\src\Algorithm;


class FoundPatterns
{


    const ELEMENTS_TO_CLEAN = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

    /**
     * @param string $enteredWord
     * @return string $eneterdWord with Dots added at begging and end.
     */
    private function addDots($enteredWord)
    {
        $wordWithAddedDots = '.' . $enteredWord . '.';
        return $wordWithAddedDots;
    }

    /**
     * @param string $fragment
     * @return string Returns fragment with removed numbers
     */
    private function cleanFragment($fragment)
    {
        $cleanfragment = str_replace(self::ELEMENTS_TO_CLEAN, "", $fragment);
        return $cleanfragment;
    }


    /**
     * @param string $enteredWord
     * @param array $wordFragments
     * @return array
     */
    protected function getPatternMatches($enteredWord, $wordFragments)
    {
        $wordWithDots = $this->addDots($enteredWord);
        $foundfragments = [];
        $foundpatternsWithPosition = array_fill(0, strlen($enteredWord) + 1, null);

        foreach ($wordFragments as $fragment) {

            $position = strpos($wordWithDots, $this->cleanFragment($fragment));
            if ($position >= 0 && $position !== false) {
                $foundpatternsWithPosition[$position] = $fragment;
                $foundfragments[] = $fragment;
            }
        }

        return $foundpatternsWithPosition;
    }


}

