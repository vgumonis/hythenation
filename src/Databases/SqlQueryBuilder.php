<?php
/**
 * Created by PhpStorm.
 * User: vilius
 * Date: 18.10.15
 * Time: 13.40
 */

namespace Hyphenation\src\Databases;

use Hyphenation\src\Databases\SqlQueryBuilderInterface;

use Hyphenation\Resources\Resources;


class SqlQueryBuilder implements SqlQueryBuilderInterface
{
    public $query;

    private $pdo;

    public function __construct()
    {

        ///print_r(Resources::getDatabaseConfig());
        $dbConfig = simplexml_load_file(Resources::getDatabaseConfig());

        $host = $dbConfig->host;;
        $db = $dbConfig->database;
        $user = $dbConfig->user;
        $pass = $dbConfig->pass;
        $charset = $dbConfig->charset;

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES => false,
        ];

        try {
            $this->pdo = new \PDO($dsn, $user, $pass, $options);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    public function __destruct()
    {
        $this->pdo = null;
    }

    public function select(array $fields = null)
    {
        if ($fields === null) {
            $this->query .= "SELECT * ";
            return $this;
        } else {

            $columns = [];

            foreach ($fields as $column) {

                $columns[] = $column;

                if ($column !== end($fields)) {
                    $columns[] = ",";
                }
            }
            $columns[] = " ";
            $columns = implode(" ", $columns);
            $this->query .= "SELECT " . $columns;

            return $this;
        }
    }

    public function from(string $tableName)
    {

        $this->query .= "FROM " . $tableName;
        return $this;
    }

    public function and()
    {

        return "AND ";

    }

    public function where(array $whereFields = null)
    {
        if ($whereFields === null) {

        } else {
            $where = [];

            foreach ($whereFields as $column => $value) {
                $where[] = $column . " = " . '"' . $value . '"';

                if ($value !== end($whereFields)) {
                    $where[] = $this->and();
                }
            }
            $where = implode(" ", $where);

            $this->query .= " WHERE (" . $where . ")";
            return $this;
        }
    }


    public function insertInto(string $tableName, array $fields)
    {

        $columns = [];

        foreach ($fields as $column) {

            $columns[] = $column;
            if ($column !== end($fields)) {
                $columns[] = ",";
            }
        }

        $columns = implode(" ", $columns);

        $this->query .= "INSERT INTO " . $tableName . " (" . $columns . ")";

        return $this;
    }

    public function value(array $valuesToInsert)
    {
        $values = [];

        foreach ($valuesToInsert as $value) {
            $values[] = "'" . $value . "'";
            if ($value !== end($valuesToInsert)) {
                $values[] = ",";
            }
        }
        $values = implode(" ", $values);

        print_r(implode(", ", $valuesToInsert));
        $this->query .= " VALUES (" . $values . ")";
        return $this;
    }

    public function deleteFrom(string $tableName)
    {
        $this->query .= "DELETE FROM " . $tableName;
        return $this;

    }


    public function update(string $tableName)

    {
        $this->query .= " UPDATE " . $tableName;
        return $this;
    }

    public function set(array $valueToInsert)
    {
        $values = [];

        foreach ($valueToInsert as $column => $value) {

            $values[] = $column . " = " . '"' . $value . '"';

            if ($value !== end($valueToInsert)) {
                $values[] = ",";
            }
        }
        $values = implode(" ", $values);

        $this->query .= " SET " . $values;
        return $this;
    }


    public function limit(int $start, int $end = null)
    {
        if ($end === null) {

            $this->query .= " LIMIT " . $start;
            return $this;
        } else

            $this->query .= " LIMIT " . $start . "," . $end;
        return $this;


    }

    public function executeQuery()
    {
        try {
//            echo "query ===";
//            print_r($this->query);
            $stmt = $this->pdo->prepare("$this->query");

            $stmt->execute();

            if (strpos($this->query, 'SELECT ') !== false) {
                $result = $stmt->fetchAll();
                return $result;
            }

        } catch (\Exception $e) {
            echo "transaction cancelled";
            $this->pdo->rollback();
        }

    }
}

