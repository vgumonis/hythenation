<?php
/**
 * Created by PhpStorm.
 * User: vilius
 * Date: 18.10.15
 * Time: 14.22
 */

namespace Hyphenation\src\Databases;


interface SqlQueryBuilderInterface
{
    public function select(array $fields = null);

    public function from(string $tableName);

    public function and();

    public function where(array $whereFields);

    public function insertInto(string $tableName, array $fields);

    public function value(array $valuesToInsert);

    public function deleteFrom(string $tableName);

    public function update(string $tableName);

    public function set(array $valueToInsert);


}
