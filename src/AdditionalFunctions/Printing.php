<?php
/**
 * Created by PhpStorm.
 * User: vilius
 * Date: 18.10.11
 * Time: 18.19
 */

namespace Hyphenation\src\AdditionalFunctions;

use Hyphenation\src\MainFunctions\HyphenatedWord;

class Printing
{


    public function printListOfHyphenatedWords($listOfHyphenatedWords)
    {
        echo "\n";
        foreach ($listOfHyphenatedWords as $testWord => $hyphenatedWord) {

            echo $testWord . ': ' . $hyphenatedWord . "\n";
        }
    }

    public function printIterationNumber()
    {
        echo "\nCycle completed with: " . HyphenatedWord::$iterationNumber . ' cycles of the syllifacation algorithm. ' . "\n";
    }


    public function printDuration($duration)
    {
        echo"\nDone in " . $duration.'seconds' ;
    }

}