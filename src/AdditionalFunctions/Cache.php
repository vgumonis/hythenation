<?php


namespace Hyphenation\src\AdditionalFunctions;

use Psr\SimpleCache\CacheInterface;
use Hyphenation\Resources\Resources;

class Cache implements CacheInterface
{


    public $cache = [];

    public static $fileCacheSwitch = false;

    public static $dbCacheSwitch = false;

    public function useCache()
    {
        //if (file_exists("Resources/cache.txt") === false) {
            if (file_exists(Resources::getCacheFile()) === false) {
            $cacheData = fopen(Resources::getCacheFile(), "a+");
            $newEntry = 'entry entry';
            fwrite($cacheData, $newEntry);
            echo "Cache file created! \n";
        } else {
            $cacheData = fopen(Resources::getCacheFile(), "a+");
            echo "Cache from file used! \n";
        }
        $cacheDataLines = explode("\n", file_get_contents(Resources::getCacheFile()));
        $cacheDataArray = [];

        foreach ($cacheDataLines as $line) {

            $lineEntry = explode(" ", $line);
            $cacheDataArray[$lineEntry[0]] = $lineEntry[1];
        }

        $this->cache = $cacheDataArray;
        fclose($cacheData);

        self::$fileCacheSwitch = true;
    }


    public function useDatabaseAsCache()
    {
        self::$dbCacheSwitch = true;
    }

    public function get($key, $default = null)
    {
        if (isset($this->cache[$key])) {
            if (in_array($this->cache[$key], $this->cache, true)) {

                return $this->cache[$key];

            } else {

                return false;
            }
        } else {
            return false;
        }
    }


    public function set($key, $value, $ttl = null)
    {
        $this->cache[$key] = $value;
        $newEntry = "\n" .$key . ' ' . $this->cache[$key];
        $cacheData = fopen(Resources::getCacheFile(), "a+");
        fwrite($cacheData, $newEntry);
        fclose($cacheData);
    }

    public function printCache()
    {
        print_r($this->cache);
    }

    public function delete($key)
    {
        // TODO: Implement delete() method.

    }

    public function clear()
    {
        // TODO: Implement clear() method.
    }

    public function getMultiple($keys, $default = null)
    {
        // TODO: Implement getMultiple() method.
    }

    public function setMultiple($values, $ttl = null)
    {
        // TODO: Implement setMultiple() method.
    }

    public function deleteMultiple($keys)
    {
        // TODO: Implement deleteMultiple() method.
    }

    public function has($key)
    {
        // TODO: Implement has() method.
    }
}