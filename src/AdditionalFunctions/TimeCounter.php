<?php

namespace Hyphenation\src\AdditionalFunctions;

class TimeCounter
{
    private $time;

    public function startTime()
    {
            $this->time = microtime(true);
    }

    public function getDuration()
    {
        $timeEnd = microtime(true);
        $duration = $timeEnd - $this->time;
        return  $duration ;
    }

}

