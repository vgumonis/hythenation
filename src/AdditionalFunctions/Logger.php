<?php

namespace Hyphenation\src\AdditionalFunctions;

use  Psr\Log\AbstractLogger;
use Hyphenation\src\MainFunctions\HyphenatedWord;

class Logger extends AbstractLogger
{


    public $logFile;

    private static $logFileIteration = 1;

    public function __construct()
    {
        $filename = 'OutputFiles/log' . self::$logFileIteration . '.txt';
        self::$logFileIteration++;
        $this->logFile = fopen($filename, "a");
    }

    /**
     *
     * Inserts message into log.txt. file.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     */
    public function log($level, $message, array $context = array())
    {
        fwrite($this->logFile, strtr($message, $context));
    }

    /**
     * Inserts time difference of then TimeCounter was instantiated and now into log.txt file.
     *
     * @param string $getTime
     */
    public function logTime($getTime)
    {
        fwrite($this->logFile, strtr($getTime, [0]));
    }

    public function logIterations()
    {
        $iteration = 'Cycle completed with: ' . HyphenatedWord::$iterationNumber . 'cycles of the  algorithm.' . "\n";
        fwrite($this->logFile, strtr($iteration, [0]));
    }

    public function logListOfHyphenatedWords($listOfHyphenatedWords)
    {
        foreach ($listOfHyphenatedWords as $testWord => $hyphenatedWord) {
            $string = $testWord . ': ' . $hyphenatedWord . "\n";
            $this->log("INFO", $string, [1, 2]);
        }
    }
}


