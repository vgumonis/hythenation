<?php

namespace Hyphenation\src\ConsoleAppController;

use Hyphenation\src\AdditionalFunctions\Logger;
use Hyphenation\src\AdditionalFunctions\Cache;
use Hyphenation\src\AdditionalFunctions\Printing;
use Hyphenation\src\MainFunctions\HyphenatedWordProxy;
use Hyphenation\src\Databases\PrebuiltSqlQueries;

class ConsoleAppController
{
    private $userInput;
    private $useCache;
    private $fragmentSource;

    private $cache;
    private $db;
    private $logger;
    private $printing;
    private $proxy;


    public function __construct()
    {
        $print = new Printing();
        $this->printing = $print;

        $database = new  PrebuiltSqlQueries();
        $this->db = $database;

        $logFile = new Logger();
        $this->logger = $logFile;

        $words = new HyphenatedWordProxy();
        $this->proxy = $words;
    }

    public function inputSource()
    {
        $answerInput = false;

        while ($answerInput === false) {
            echo "\nChoose source of words to syllabify:\n 'u' - Enter word manually. \n 'f' - Enter words from file.";
            echo "\n 'd' - Enter words from database. \n";
            $input = readline("");
            switch ($input) {
                case 'u':
                    $this->userInput = 'u';
                    $answerInput = true;
                    break;
                case 'f':
                    $this->userInput = 'f';
                    $answerInput = true;
                    break;
                case 'd':
                    $this->userInput = 'd';
                    $answerInput = true;
                    break;
                default:
                    $answerInput = false;
                    break;
            }
        }
    }

    public function fragmentSource()
    {
        $answerFragments = false;

        while ($answerFragments === false) {
            echo "Choose word fragment source:\n 'f' - From file.\n 'd' - From database.\n";
            $input = readline("");
            switch ($input) {
                case 'f':
                    $this->fragmentSource = 'f';
                    $answerFragments = true;
                    break;
                case 'd':
                    $this->fragmentSource = 'd';
                    $answerFragments = true;
                    break;
                default:
                    break;
            }
        }
    }

    public function cacheSource()
    {
        $answerCache = false;

        while ($answerCache === false) {
            echo "Choose cache source:\n 'n' - Do not use cache. \n 'f' - Use cache from file. \n 'd' - Use cache from database.\n";
            $input = readline("");
            switch ($input) {
                case 'n':
                    $this->useCache = 'n';
                    $answerCache = true;
                    break;
                case 'f':
                    $this->useCache = 'f';
                    $answerCache = true;
                    break;
                case 'd':
                    $this->useCache = 'd';
                    $answerCache = true;
                    break;
                default:
                    break;
            }
        }
    }

    public function runAlgorithm()
    {
        $this->executeCache();

        $hyphenatedWords = $this->proxy->getHyphenatedWordsList($this->userInput, $this->fragmentSource);
        $this->printInfo($hyphenatedWords);
        $this->logInfo($hyphenatedWords);
    }

    private function executeCache()
    {
        switch ($this->useCache) {
            case ("n"):
                break;
            case ("d"):
                $this->cache = new Cache();
                $this->cache->useDatabaseAsCache();
                break;
            case ("f"):
                $this->cache = new Cache();
                $this->cache->useCache();
                break;
        }
    }

    private function printInfo($words)
    {
        $this->printing->printListOfHyphenatedWords($words);
        $this->printing->printIterationNumber();
        $this->printing->printDuration(HyphenatedWordProxy::$time->getDuration());
    }

    private function logInfo($words)
    {
        $this->logger->logListOfHyphenatedWords($words);
        $this->logger->logIterations();
        $this->logger->logTime(HyphenatedWordProxy::$time->getDuration());
    }


    ////Database Helpers!

    public function resetWordFragments()
    {

        $this->db->deletePattersTable();
        $this->db->uploadPatterns();
    }

    public function resetHyphenatedWords()
    {
        $this->db->deleteHyphenationTable();
        $this->db->uploadHyphenatedWords($this->proxy->getHyphenatedWordsList($this->userInput, $this->fragmentSource));
    }

    public function resetWordsToTest()
    {
        $this->db->deleteWordsToTestTable();
        $this->db->uploadWordsToTest();
    }
}
