<?php

namespace Hyphenation\src\API;

use Couchbase\RegexpSearchQuery;
use http\Env\Request;

use Hyphenation\src\Algorithm\SyllabificatedWord;
use Hyphenation\src\Databases\SqlQueryBuilder;
use Hyphenation\Resources\Resources;
use PhpParser\Node\Expr\Print_;


class WebApi implements ApiInterface
{

    public function useApi()
    {
        if (isset($_SERVER['REQUEST_METHOD'])) {
            switch ($_SERVER['REQUEST_METHOD']) {
                case 'GET':
                    return $this->get();
                    break;
                case 'POST':
                    $this->post();
                    break;
                case 'PUT':
                    $this->put();
                    break;
                case 'DELETE':
                    echo "delete";
                    $this->delete();
                    break;
            }
        }
    }

    public function get()
    {
        $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = parse_url($url, PHP_URL_QUERY);
        $uri = explode("/", $url);

        $query = new SqlQueryBuilder();
        if ($uri[1] === "all_words") {
            $query->select()->from("hyphenation");

            return $all = $query->executeQuery();


        }else {
            $query->select()->from("hyphenation")->where(["word" => $uri[1]]);
             $all = $query->executeQuery();

             ////var_dump($all);
             return $all;



        }
    }


    public function post()
    {
        $hyphentedWord = new SyllabificatedWord();
        $hyphentedWord = $hyphentedWord->hyphenate($_POST["word"], explode("\n", file_get_contents(Resources::getFragmentFile())));
        $time = date("Y-m-d H:i:s");
        $query = new SqlQueryBuilder();

        $query->insertInto("hyphenation", ["word", "hyphenated_word", "added_on"])->value([$_POST["word"], $hyphentedWord, $time]);
        $query->executeQuery();
    }

    public function put()
    {
        $time = date("Y-m-d H:i:s");
        $entityBody = file_get_contents('php://input');
        //print_r($entityBody);
        $body = explode("=", $entityBody);
        $query = new SqlQueryBuilder();

        $hyphentedWord = new SyllabificatedWord();
        $hyphentedWord = $hyphentedWord->hyphenate($body[1], explode("\n", file_get_contents(Resources::getFragmentFile())));

        $query->update("hyphenation")->set(["word" => $body[1], "hyphenated_word" => $hyphentedWord, "added_on" => $time])->where(['word' => $body[0],]);
        $query->executeQuery();

        echo "PUT!";
    }

    public function delete()
    {
        $body = file_get_contents("php://input");
        $query = new SqlQueryBuilder();

        $query->deleteFrom("hyphenation")->where(["word" => $body]);
        $result =  $query->executeQuery();

        //return var_dump($result);

        //echo "DELETE";
    }

    public function test()
    {
        print_r("Test");
    }

    public function getPage ($start)
    {
        $query = new SqlQueryBuilder();

        $query->select()->from("hyphenation")->limit($start, 10);
        return  $query->executeQuery();
    }
}





