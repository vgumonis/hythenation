<?php
/**
 * Created by PhpStorm.
 * User: vilius
 * Date: 18.10.11
 * Time: 15.19
 */

namespace Hyphenation\src\API;


interface ApiInterface
{
    public function useApi();
    public function get();
    public function post();
    public function put();
    public function delete();
}