<?php

namespace Hyphenation\src\API;

use Couchbase\RegexpSearchQuery;
use http\Env\Request;

use Hyphenation\src\Databases\SqlQueryBuilder;


class Api implements ApiInterface
{

    public function useApi()
    {
        if (isset($_SERVER['REQUEST_METHOD'])) {
            switch ($_SERVER['REQUEST_METHOD']) {
                case 'GET':
                    $this->get();
                    break;
                case 'POST':
                    $this->post();
                    break;
                case 'PUT':
                    $this->put();
                    break;
                case 'DELETE':
                    echo "delete";
                    $this->delete();
                    break;
            }
        }
    }

    public function get()
    {
        $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $uri = parse_url($url, PHP_URL_QUERY);
        $querysForSQL = [];
        $uri = explode("&", $uri);

        foreach ($uri as $querys) {
            $query = explode("=", $querys);
            $querysForSQL[$query[0]] = $query[1];
        }
        $tableName = $querysForSQL["tableName"];

        $sql = [];
        foreach (array_splice($querysForSQL, 1) as $key => $value) {
            $sql[$key] = $value;
        }

        $query = new SqlQueryBuilder();
        $query->select()->from($tableName)->where($sql);
        $all = $query->executeQuery();
        $jsonAll = json_encode($all);
        echo "GET";
        print_r($jsonAll);
        return $jsonAll;

    }


    public function post()
    {
        $time = date("Y-m-d H:i:s");

        $query = new SqlQueryBuilder();
        $query->insertInto($_POST["tableName"], ["word", "hyphenated_word", "added_on"])->value([$_POST["word"], $_POST["hyphenated_word"], $time]);
        $query->executeQuery();
        echo "POST!";
    }

    public function put()
    {
        $time = date("Y-m-d H:i:s");
        $entityBody = json_decode(file_get_contents('php://input'), true);

        $query = new SqlQueryBuilder();
        foreach ($entityBody as $body) {
            $query->update(($body['tableName']))->set(["word" => $body['word'], "hyphenated_word" => $body['hyphenated_word'], "added_on" => $time])->where(['word' => $body['whereWord']]);
            $query->executeQuery();
        }
        echo "PUT!";
        //, "added_on" => $time
    }

    public function delete()
    {
        $entityBody = json_decode(file_get_contents('php://input'), true);

        $query = new SqlQueryBuilder();
        foreach ($entityBody as $body) {
            $query->deleteFrom($body['tableName'])->where([$body['DeleteByColumn'] => $body['word']]);
            $query->executeQuery();
        }
        echo "DELETE";
    }


}
