<?php
/**
 * Created by PhpStorm.
 * User: vilius
 * Date: 18.10.15
 * Time: 11.14
 */

namespace Hyphenation\src\MainFunctions;


use Hyphenation\src\AdditionalFunctions\TimeCounter;

class HyphenatedWordProxy extends HyphenatedWord
{
    private $hyphenatedWordList = null;

    public static $time;

    public function setHyphenatedWordList(string $wordSource, string $fragmentSource)
    {
        $this->hyphenatedWordList = new HyphenatedWord();
        $this->hyphenatedWordList->inputWordsAndFragments($wordSource, $fragmentSource);

        self::$time = new TimeCounter();
        self::$time->startTime();

        $this->hyphenatedWordList->setHyphenatedWords();
    }


    public function getHyphenatedWordsList(string $wordSource, string $fragmentSource)
    {

        if ($this->hyphenatedWordList === null) {
            $this->setHyphenatedWordList($wordSource, $fragmentSource);
            $this->hyphenatedWordList->getHyphenatedWords();
            return $this->hyphenatedWordList->getHyphenatedWords();
        } else {

            return $this->hyphenatedWordList->getHyphenatedWords();
        }
    }
}