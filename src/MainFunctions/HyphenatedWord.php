<?php

namespace Hyphenation\src\MainFunctions;

use Hyphenation\src\Algorithm\SyllabificatedWord;
use Hyphenation\src\AdditionalFunctions\Cache;
use Hyphenation\src\Databases\PrebuiltSqlQueries;

class HyphenatedWord
{
    private $wordFragments;

    private $testWords;

    protected $listOfHyphenatedWords;

    public static $iterationNumber;


    public function inputWordsAndFragments(string $sourceWords, string $sourceFragments)
    {
        $input = new Input();

        $this->testWords = $input->setWords($sourceWords);

        $this->wordFragments = $input->setFragments($sourceFragments);
    }

    public function getWordsToTest()
    {
        return $this->testWords;
    }

    public function getWordFragments()
    {
        return $this->wordFragments;
    }

    public function getHyphenatedWords()
    {
        return $this->listOfHyphenatedWords;
    }

    public function setHyphenatedWords()
    {
        if (Cache::$fileCacheSwitch === true) {
            $this->setHyphenatedWordsUsingCache();
        } elseif (Cache::$dbCacheSwitch === true) {
            $this->setHyphenatedWordsUsingDatabase();
        } else {
            $this->setHyphenatedWordsWithoutCache();
        }
    }

    private function setHyphenatedWordsWithoutCache()
    {
        $answer = new SyllabificatedWord();
        $listOfHyphenatedWords = [];
        $iterationCount = 0;

        foreach ($this->testWords as $testWord) {
            $hyphenatedWord = $answer->hyphenate($testWord, $this->wordFragments);
            $listOfHyphenatedWords[$testWord] = $hyphenatedWord;
            $iterationCount++;
        }
        ///$this->testedWordWithFoundPatterns = $answer->getTestedWordWithFoundPatterns();
        self::$iterationNumber = $iterationCount;
        $this->listOfHyphenatedWords = $listOfHyphenatedWords;
    }

    private function setHyphenatedWordsUsingCache()
    {
        $answer = new SyllabificatedWord();
        $listOfHyphenatedWords = [];
        $iterationCount = 0;

        $cache = new Cache();
        $cache->useCache();
        foreach ($this->testWords as $testWord) {

            if ($cache->get($testWord) !== false) {
                $listOfHyphenatedWords[$testWord] = $cache->get($testWord);

            } else {
                $hyphenatedWord = $answer->hyphenate($testWord, $this->wordFragments);
                $cache->set($testWord, $hyphenatedWord);
                $listOfHyphenatedWords[$testWord] = $hyphenatedWord;
                $iterationCount++;
            }
        }
        self::$iterationNumber = $iterationCount;
        $this->listOfHyphenatedWords = $listOfHyphenatedWords;
    }

    private function setHyphenatedWordsUsingDatabase()
    {
        $answer = new SyllabificatedWord();
        $listOfHyphenatedWords = [];
        $iterationCount = 0;
        echo "Database used as cache!";

        $db = new PrebuiltSqlQueries();

        foreach ($this->testWords as $testWord) {
            $dbEntry = $db->getWord($testWord);

            if ($dbEntry !== false) {
                $dbEntryHyphenated = $dbEntry[1];
                $listOfHyphenatedWords[$testWord] = $dbEntryHyphenated;
            } else {
                $hyphenatedWord = $answer->hyphenate($testWord, $this->wordFragments);
                $db->setWord($testWord, $hyphenatedWord);
                $listOfHyphenatedWords[$testWord] = $hyphenatedWord;
                $iterationCount++;
            }
        }
        self::$iterationNumber = $iterationCount;
        $this->listOfHyphenatedWords = $listOfHyphenatedWords;
    }
}