<?php
/**
 * Created by PhpStorm.
 * User: vilius
 * Date: 18.10.12
 * Time: 13.50
 */

namespace Hyphenation\src\MainFunctions;


use Hyphenation\src\Databases\PrebuiltSqlQueries;
use Hyphenation\src\Databases\SqlQueryBuilder;
use Hyphenation\src\MainFunctions\inputInterface;
use Hyphenation\Resources\Resources;

class Input implements inputInterface
{

    public function setWordFromUser()
    {
        echo 'Enter word:  ';
        $wordFromUser[] = readline("");
        return $wordFromUser;
    }

    public function setWordsFromFile()
    {
        $words = explode("\n", file_get_contents(Resources::getWordFile()));
        return $words;
    }

    public function setWordsFromDataBase()
    {
        $db = new PrebuiltSqlQueries();
        return $db->getTestWordsFromDb();
    }

    public function setWordFragmentsFromFile()
    {
        return explode("\n", file_get_contents(Resources::getFragmentFile()));
    }

    public function setWordFragmentsFromDatabase()
    {
        $db = new PrebuiltSqlQueries();
        return $db->getPatternsFromDb();
    }


    public function setWords(string $source)
    {
        switch ($source) {
            case "d":
                return $this->setWordsFromDataBase();
                break;
            case "f":
                return $this->setWordsFromFile();
                break;
            case "u":
                return $this->setWordFromUser();
                break;
        }
    }


    public function setFragments(string $source)
    {
        switch ($source) {
            case "f":
                return $this->setWordFragmentsFromFile();
                break;
            case "d":
                return $this->setWordFragmentsFromDatabase();
                break;
        }
    }


}