<?php

namespace Hyphenation\src\MainFunctions;

interface inputInterface
{

    public function setWordFromUser();

    public function setWordsFromFile();

    public function setWordsFromDataBase();

    public function setWordFragmentsFromFile();

    public function setWordFragmentsFromDatabase();
}