<?php
/**
 * Created by PhpStorm.
 * User: vilius
 * Date: 18.10.19
 * Time: 13.17
 */

namespace Hyphenation\WebApp\Models;

use Hyphenation\src\API\WebApi;

use Hyphenation\WebApp\WebControllers\ViewAllController;

class ViewAllModel
{
    public function __construct ($start)
    {
        $api = new WebApi();
        ViewAllController::$pageContent = $api->getPage($start);
    }
}