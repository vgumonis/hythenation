<?php

namespace Hyphenation\WebApp\Models;

use Hyphenation\src\API\WebApi;

class FindWordModel
{
    public static $foundWordContent= null;

    public function __construct()
    {
        $api = new WebApi();
        $words = $api->useApi();

        if (empty($words)) {
            return false;
        }

        self::$foundWordContent = $words;

    }
}





