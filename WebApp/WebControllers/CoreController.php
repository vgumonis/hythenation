<?php


namespace Hyphenation\WebApp\WebControllers;

use function DI\string;

require_once '../../vendor/autoload.php';


$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$uri = parse_url($url, PHP_URL_QUERY);

$uri = explode("/", $uri);

$controller = "Hyphenation\WebApp\WebControllers\\".$uri[0] .'Controller';


if (isset($uri[0])) {

    if ($uri[0] === "ViewAllController") {
        ViewAllController::$start = $uri[1];
        ViewAllController::$page = round(($uri[1] / 10));
        ViewAllController::getPage(ViewAllController::$start);

    } else {
       new $controller();
    }
}


