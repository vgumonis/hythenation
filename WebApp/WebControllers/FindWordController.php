<?php

namespace Hyphenation\WebApp\WebControllers;


use Hyphenation\WebApp\Views\FoundWordView;

use Hyphenation\WebApp\Models\FindWordModel;

class FindWordController
{
    public function __construct()
    {
        new FindWordModel();

        new FoundWordView();
    }
}





