<?php
/**
 * Created by PhpStorm.
 * User: vilius
 * Date: 18.10.19
 * Time: 13.17
 */

namespace Hyphenation\WebApp\WebControllers;

use Hyphenation\WebApp\Views\AllWordView;

use Hyphenation\WebApp\Models\ViewAllModel;

class ViewAllController
{
    public static $start = 0;

    public static $page = 0;

    public static $pageContent;

    public static function getPage($start)
    {
        new ViewAllModel($start);

        new AllWordView();
    }
}