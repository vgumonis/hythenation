<?php
/**
 * Created by PhpStorm.
 * User: vilius
 * Date: 18.10.23
 * Time: 15.54
 */

namespace Hyphenation\WebApp\WebControllers;

use Hyphenation\WebApp\Views\MainView;


class MainViewController
{
    public function __construct()
    {
        new MainView();
    }
}