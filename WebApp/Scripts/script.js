function findWord() {
    httpRequest = new XMLHttpRequest();

    if (checkIfEmpty(document.getElementById('find_input').value) !== true) {
        return false;
    }

    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    httpRequest.onreadystatechange = displayFind;
    httpRequest.open('GET', 'WebApp/WebControllers/CoreController.php?FindWord/' + document.getElementById('find_input').value, true);
    httpRequest.send();


}

function displayFind() {

    if (httpRequest.readyState === XMLHttpRequest.DONE) {
        if (httpRequest.status === 200 && httpRequest.responseText !== '') {

            document.getElementById("find_ok").style.display = "block";
            document.getElementById("found_word").innerHTML = (httpRequest.responseText);
            resetForm(document.getElementById('find_input'));

            getAll();
            setTimeout(function () {
                document.getElementById("find_ok").style.display = "none"
            }, 3000);
            setTimeout(function () {
                document.getElementById("found_word").style.display = "none"
            }, 3000);

        } else {
            document.getElementById("find_error").style.display = "block";
            alert('There was a problem with the request.');
        }
    }

}


function addWord() {
    httpRequest = new XMLHttpRequest();

    if (checkIfEmpty(document.getElementById('add_input').value) !== true) {
        return false;
    }

    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    httpRequest.onreadystatechange = displayAdd;
    httpRequest.open("POST", 'WebApp/WebControllers/CoreController.php?AddWord/', true);
    httpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    httpRequest.send("word=" + document.getElementById('add_input').value);
}


function displayAdd() {

    if (httpRequest.readyState === XMLHttpRequest.DONE) {
        if (httpRequest.status === 200) {
            document.getElementById("add_ok").style.display = "block";
            ///document.getElementById("add_ok").innerHTML = (httpRequest.responseText);
            resetForm(document.getElementById('add_input'));
            setTimeout(function () {
                document.getElementById("add_ok").style.display = "none"
            }, 3000);
            getAll();


        } else {
            document.getElementById("add_error").style.display = "block";
            alert('There was a problem with the request.');
        }
    }
}


function deleteWord() {
    httpRequest = new XMLHttpRequest();

    if (checkIfEmpty(document.getElementById('delete_input').value) !== true) {
        return false;
    }

    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    httpRequest.onreadystatechange = displayDelete;
    httpRequest.open("DELETE", 'WebApp/WebControllers/CoreController.php?DeleteWord/', true);
    httpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    httpRequest.send(document.getElementById('delete_input').value);
}


function displayDelete() {

    if (httpRequest.readyState === XMLHttpRequest.DONE) {
        if (httpRequest.status === 200) {
            document.getElementById("delete_ok").style.display = "block";
            ///document.getElementById("delete_ok").innerHTML = (httpRequest.responseText);
            resetForm(document.getElementById('delete_input'));
            setTimeout(function () {
                document.getElementById("delete_ok").style.display = "none"
            }, 3000);
            getAll();

        } else {
            document.getElementById("delete_error").style.display = "block";
            alert('There was a problem with the request.');
        }
    }
}

function updateWord() {
    httpRequest = new XMLHttpRequest();

    if (checkIfEmpty(document.getElementById('old_word').value) !== true && checkIfEmpty(document.getElementById('new_word').value) !== true) {
        return false;
    }
    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    httpRequest.onreadystatechange = displayUpdate;
    httpRequest.open("PUT", 'WebApp/WebControllers/CoreController.php?UpdateWord/', true);
    httpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    httpRequest.send(document.getElementById('old_word').value + '=' + document.getElementById('new_word').value);
}


function displayUpdate() {

    if (httpRequest.readyState === XMLHttpRequest.DONE) {
        if (httpRequest.status === 200) {
            document.getElementById("update_ok").style.display = "block";
            //document.getElementById("update_ok").innerHTML = (httpRequest.responseText);
            resetForm(document.getElementById('old_word'));
            resetForm(document.getElementById('new_word'));
            setTimeout(function () {
                document.getElementById("update_ok").style.display = "none"
            }, 3000);
            getAll();

        } else {
            document.getElementById("update_error").style.display = "block";
            alert('There was a problem with the request.');
        }
    }

}

function getAll() {
    httpRequest = new XMLHttpRequest();

    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    httpRequest.onreadystatechange = displayContents;
    httpRequest.open('GET', 'WebApp/WebControllers/CoreController.php?ViewAllController/0', true);
    httpRequest.send();
}

function displayContents() {

    if (httpRequest.readyState === XMLHttpRequest.DONE) {
        if (httpRequest.status === 200) {

            document.getElementById("all_words").innerHTML = (httpRequest.responseText);

        } else {
            alert('There was a problem with the request.');
        }
    }
}

function getPageNumber(number) {
    httpRequest = new XMLHttpRequest();

    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    httpRequest.open('GET', 'WebApp/WebControllers/CoreController.php?ViewAllController/' + number, true);
    //console.log(number);
    httpRequest.send();
    httpRequest.onreadystatechange = displayContents;
}

function checkIfEmpty(field) {
    if (field.length === 0) {
        alert('Enter something!');
        return false;
    } else {
        return true;
    }
}


function resetForm(form) {
    form.value = '';
}


function deleteWordFromList(word) {
    httpRequest = new XMLHttpRequest();

    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }

    httpRequest.onreadystatechange = displayDelete;
    httpRequest.open("DELETE", 'WebApp/WebControllers/CoreController.php?DeleteWord/', true);
    httpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    httpRequest.send(word);
}