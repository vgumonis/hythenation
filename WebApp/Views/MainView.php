<?php
namespace Hyphenation\WebApp\Views;

class MainView
{
    public function __construct()
    {
        ?>
        <!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            <title>Hyphenation</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
                  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
                  crossorigin="anonymous">
            <link rel="stylesheet" href="WebApp/Style/styles.css">
        </head>
        <body onload="getAll()">

        <?php new FunctionsView() ?>

        <script src="WebApp\Scripts\script.js"></script>
        </body>
        </html>
        <?php
    }
}