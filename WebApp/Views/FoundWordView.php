<?php
/**
 * Created by PhpStorm.
 * User: vilius
 * Date: 18.10.23
 * Time: 17.16
 */

namespace Hyphenation\WebApp\Views;

use Hyphenation\WebApp\Models\FindWordModel;


class FoundWordView
{
    public function __construct()
    {
        $words = FindWordModel::$foundWordContent;

        echo '<html>';
        echo '<table class=" table table-striped table-bordered one-word">';
        echo '<tr>';
        echo '<th>ID</th>';
        echo '<th>Word</th>';
        echo '<th>Hyphenated Word</th>';
        echo '<th>Added On</th>';
        echo '</tr>';

        foreach ($words as $word) {
            echo '<tr>';
            echo "<td>" . $word["id"] . "</td>";
            echo "<td>" . $word["word"] . "</td>";
            echo "<td>" . $word["hyphenated_word"] . "</td>";
            echo "<td>" . $word["added_on"] . "</td>";
            echo '</tr>';
        }
        echo '</table>';
        echo '</html>';
    }
}