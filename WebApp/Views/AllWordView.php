<?php
namespace Hyphenation\WebApp\Views;
use Hyphenation\WebApp\WebControllers\ViewAllController;
class AllWordView
{
    public function __construct()
    {

        $words = ViewAllController::$pageContent;

        echo '<table class=" table table-striped table-bordered all-words">';
        echo '<tr>';
        echo '<th>ID</th>';
        echo '<th>Word</th>';
        echo '<th>Hyphenated Word</th>';
        echo '<th>Added On</th>';
        echo '</tr>';

        foreach ($words as $word) {
            echo '<tr>';
            echo "<td>" . $word["id"] . "</td>";
            echo "<td>" . $word["word"] . "</td>";
            echo "<td>" . $word["hyphenated_word"] . "</td>";
            echo "<td>" . $word["added_on"] . "</td>";
            echo '</tr>';
        }

        echo '</table>';

        echo '<ul class="pagination">';

        if (ViewAllController::$page == 0) {

            echo '<li  class="page-item"><a id="previous" class="page-link btn disabled" href="#">Previous</a></li>';

            echo '<li  class="page-item"><a  id="first" class="page-link btn disabled" href="#">' . (ViewAllController::$page) . '</a></li>';

        } else {

            echo '<li onclick="getPageNumber(' . (ViewAllController::$start - 10) . ')" class="page-item"><a id="previous" class="page-link " href="#">Previous</a></li>';

            echo '<li onclick="getPageNumber(' . (ViewAllController::$start - 10) . ')" class="page-item"><a  id="first" class="page-link" href="#">' . (ViewAllController::$page) . '</a></li>';
        }
        echo '<li onclick="getPageNumber(' . (ViewAllController::$start) . ')" class="page-item active"><a id="second" class="page-link" href="#">' . (ViewAllController::$page + 1) . '</a></li>';

        echo '<li onclick="getPageNumber(' . (ViewAllController::$start + 10) . ')" class="page-item" ><a id="third" class="page-link" href="#">' . (ViewAllController::$page + 2) . '</a></li>';

        echo '<li onclick="getPageNumber(' . (ViewAllController::$start + 10) . ')" class="page-item" ><a id="next" class="page-link" href="#">Next</a></li>';

        echo '</ul>';

    }
}