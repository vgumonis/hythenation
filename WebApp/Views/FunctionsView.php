<?php

namespace Hyphenation\WebApp\Views;


class FunctionsView
{
    public function __construct()
    {
        ?>
        <h1> Welcome to
            <div style="font-style: italic"> Hyphenation 3000</div>
        </h1>
        <div class="container">
            <div class="wrapper row row-centered">
                <div class="col-sm-6 col-centered left">

                    <form class="form-text">
                        Add word: <br>
                        <input id="add_input" type="text" name="word_to_add" class="form"
                               placeholder=" Enter word to delete">
                        <button type="button" class="btn btn-primary button" onclick="addWord()">Add Word</button>

                        <div class="status">
                            <p id="add_ok" class="ok">
                                Word added!
                            </p>
                            <p id="add_error" class="error">
                                Whoops!
                            </p>
                        </div>


                    </form>


                    <form class="form-text">
                        Find word: <br>
                        <input id="find_input" type="text submit" name="word_to_find" class="form"
                               placeholder=" Enter word to delete">

                        <button type="button" class="btn btn-primary button" onclick="findWord()">Find Word</button>


                        <div class="status">
                            <p id="find_ok" class="ok">
                                Word found!
                            </p>
                            <p id="found_word">
                            </p>
                            <p id="find_error" class="error">
                                Whoops!
                            </p>
                        </div>

                    </form>


                    <form class="form-text">
                        Update word: <br>
                        <input id="old_word" type="text" name="word_to_update" class="form"
                               placeholder=" Enter word to update"><br>
                        <input id="new_word" type="text" name="updated_word" class="form"
                               placeholder=" Enter updated word">

                        <button type="button" class="btn btn-primary button" onclick="updateWord()">Update Word</button>

                        <div id="Update_status" class="status">
                            <p id="update_ok" class="ok">
                                Word updated!
                            </p>
                            <p id="update_error" class="error">
                                Whoops!
                            </p>
                        </div>

                    </form>


                    <form class="form-text">
                        Delete word: <br>
                        <input id="delete_input" type="text" name="word_to_delete" class="form"
                               placeholder=" Enter word to delete">

                        <button type="button" class="btn btn-primary button" onclick="deleteWord()">Delete Word</button>

                        <div class="status">
                            <p id="delete_ok" class="ok">
                                Word deleted!
                            </p>
                            <p id="delete_error" class="error">
                                Whoops!
                            </p>
                        </div>

                    </form>


                    <!--        <button type="button" onclick="makeRequest()">Make a request</button>-->

                </div>


                <div class="col-sm-6 col-centered">
                    <div id="all_words"></div>
                </div>


            </div>

        </div>


        <?php
    }
}

?>