<?php

require_once 'vendor/autoload.php';

use Hyphenation\WebApp\WebControllers\MainViewController;
use Hyphenation\WebApp\WebControllers\ErrorController;

if(file_exists("WebApp/Views/MainView.php")) {

    new MainViewController();

} else {

    $error = new ErrorController();
    $error->pageNotFound();
}

